package dao;
import model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Status;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import model.Note;
@Stateless
@Local(DaoInterFace.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class NoteDAO implements DaoInterFace {
	
	@Resource(lookup = "jdbc/oracle")
	private DataSource ds;
	
	@Resource
	private UserTransaction ut;
	
	@Override
	public Note create(String uId, String uText, Date uCreatedate,Status uStatus) {
           Note ret= null;
           String valueOfDate = uCreatedate.toString();
		try(PreparedStatement stmt= ds.getConnection().prepareStatement("Insert into notess (id, text) values (?,?)")){
			ut.begin();
			//ret=new Note(uId, uText, valueOfDate, uStatus);
			//ret= note.getText();
			stmt.setString(1, uId);
			stmt.setString(2, uText);
			stmt.setString(3, valueOfDate);
			stmt.setString(4, uStatus.toString());
			stmt.executeQuery();
			ut.commit();
		}
		catch(Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return ret;
	}
	

	@Override
	public Note retrieve() {
		Note item = null;
        try(Statement pstmt= ds.getConnection().createStatement()){

			
			pstmt.execute("Select * from notesorai");
			
			
			try (ResultSet rs = pstmt.getResultSet()){
				    while (rs.next()) {

					 item = new Note(rs.getString("id"), rs.getString("text"), 
							             rs.getDate("createdate"), Status.valueOf(rs.getString("status")));
										
				}
			}

		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return item;
	}

	@Override
	public void update(String id) {
		
		try(PreparedStatement stmt= ds.getConnection().prepareStatement("Insert into notess (id) values (?)")){
			ut.begin();
			//ret=note;
			//ret= note.getText();
			stmt.setString(1, id);
		
			stmt.executeUpdate();
			ut.commit();
		}
		catch(Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	@Override
	public void delete(String id) {
		String ret= null;
		
		try(PreparedStatement stmt= ds.getConnection().prepareStatement("Delete from notess where id=?")){
			ut.begin();
			stmt.setString(1, id);
			
			stmt.executeUpdate();
			ut.commit();
		}
		catch(Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		
	}

	@Override
	public List<Note> retrieveAll() {


		List<Note> ret=new ArrayList<Note>();

		try(Statement pstmt= ds.getConnection().createStatement()){

			
			pstmt.execute("Select * from notesorai");
			
			
			try (ResultSet rs = pstmt.getResultSet()){
				    while (rs.next()) {

					Note item = new Note(rs.getString("id"), rs.getString("text"), 
							             rs.getDate("createdate"), Status.valueOf(rs.getString("status")));
										 ret.add(item);
				}
			}

		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	
	}


