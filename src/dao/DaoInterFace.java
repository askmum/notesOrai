package dao;

import java.util.Date;
import java.util.List;

import model.Note;
import model.Status;

public interface DaoInterFace {
	
   Note	create(String uId, String uText, Date uCreatedate,Status uStatus);
   Note retrieve();
   void update(String id);
   void delete(String id);
   List<Note> retrieveAll();


}
