package model;

import java.util.Date;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
public class Note {
	
	private String id;
	private String text;
	private Date date;
	private Status status;
	
	
	
	
	public Note() {
		super();
	}
	public Note(String text, Date date, Status status) {
		super();
		this.id=UUID.randomUUID().toString();
		this.text = text;
		this.date = date;
		this.status = status;
	}
	public Note(String id, String text, Date date, Status status) {
		super();
		this.id = id;
		this.text = text;
		this.date = date;
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Enum getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	

}
