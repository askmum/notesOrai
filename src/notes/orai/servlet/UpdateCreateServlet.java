package notes.orai.servlet;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoInterFace;
import model.Status;

/**
 * Servlet implementation class UpdateCreateServlet
 */
@WebServlet("/UpdateCreateServlet")
public class UpdateCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	@Inject
    private DaoInterFace notes;
	
	
    public UpdateCreateServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = (String) request.getAttribute("id");
		notes.update(id);
		response.getWriter().println("Sikeres volt a törlés");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String id = (String) request.getAttribute("id");
		String text = (String) request.getAttribute("text");
		Date date = (Date) request.getAttribute("createdate");
		Status status = (Status) request.getAttribute("status");
		notes.create(id, text, date, status);
		response.getWriter().println("Sikeres volt a Note készítés");
		
	}

}
